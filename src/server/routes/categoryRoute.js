'use strict';

var express = require('express');
var Q = require('q');
var categoryService = require('../services/categoryService');
var db = require('../database/dbConnection');

var router = express.Router();

router.get('/', function (req, res, next) {
    var connection = null;

    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return categoryService.getCategories(connection);
    })
    .then(function (data) {
         res.json(data);
    })
    .catch(function (err) {
        next(err);
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

router.get('/:id', function (req, res, next) {
    var connection = null,
        id = req.params.id;

    db.getConnection()
    .then(function (conn) {
        connection = conn;
        return categoryService.getCategory(connection, id);
    })
    .then(function(data) {
        res.json(data[0]);
    })
    .catch(function (err) {
        next(err);
    })
    .finally(function(){
        if(connection) connection.release();
    })
    .done();
});

module.exports = router;