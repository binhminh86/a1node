'use strict';

var ProductService = {};

ProductService.getProducts = function getProducts(connection, category) {
    var sql;
    if(category) {
        sql =  "SELECT p.id, p.name, p.thumbnail, p.price FROM product p INNER JOIN category c ON p.category_id = c.id WHERE c.name = :categoryName";
        return connection.query(sql, { categoryName: category });
    } else {
        sql = "SELECT id, name, thumbnail, price FROM product";
        return connection.query(sql);
    }
};

ProductService.getProduct = function getProduct(connection, id) {
    var sql = "SELECT id, name, thumbnail, price, description FROM product where id = :id";
    return connection.query(sql, { id : id });
}

module.exports = ProductService;